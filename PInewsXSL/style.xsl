<!--
            Punto-Informatico.It XSL Releases Roller v0.1

	Questo file XSL ha il semplice scopo di creare un listing HTML
    automatico dei vari RSS feeds del sito Punto-Informatico.it.

    Ho inserito inoltre un id e delle classi per poterci applicare
    delle trasformazioni usando un CSS (Cascading Style Sheets) ed
	la variabile XSLT "immagine" per scegliere se visualizzare anche
	l'immagine o meno.

                  CSS                 	     TAG HTML
          #PI_RSS_Reader                        dl
          .PI_RSS_Reader_Index_Title            di (Rubrica)
          .PI_RSS_Reader_Index                  di (Articolo)
          .PI_RSS_Reader_Description            dd (Articolo)

    Copyright (C) 2007  Filippo Baruffaldi (www.baruffaldi.info)

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You can find a copy at this address: http://www.gnu.org/licenses
-->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

		<!-- Inserisci 0 se non vuoi che venga visualizzata anche l'immagine di PI -->
		<xsl:variable name="immagine">1</xsl:variable>

		<xsl:template match="/">
						<dl id="PI_RSS_Reader">
								<xsl:if test="$immagine = 1">
										<a href="{rss/channel/image/link}" title="{rss/channel/image/title}"><img style="width: {rss/channel/image/width}; height: {rss/channel/image/height};" src="{rss/channel/image/url}" alt="{rss/channel/title}" /></a>
								</xsl:if>
								<xsl:comment>
										<xsl:text>Rubrica: </xsl:text><xsl:value-of select="rss/channel/title" />
								</xsl:comment>
								<di class="PI_RSS_Reader_Index_Title"><h3><xsl:value-of select="rss/channel/title" /></h3></di>
								<dd><br /></dd>
								<xsl:apply-templates select="rss/channel" />
						</dl>
		</xsl:template>

		<xsl:template match="rss/channel">
						<xsl:for-each select="item">
								<xsl:comment>
										<xsl:text>Articolo: </xsl:text><xsl:value-of select="title" />
								</xsl:comment>
								<di class="PI_RSS_Reader_Index"><a href="{link}" title="{title}"><xsl:value-of select="title" /></a></di>
								<dd class="PI_RSS_Reader_Description"><xsl:value-of select="description" /></dd>
								<dd><br /></dd>
						</xsl:for-each>
		</xsl:template>

</xsl:stylesheet>
