<?php
/*************************************************************************/
/*************************************************************************/
/**          Punto-Informatico.It XML+XSL Releases Roller v0.2          **/
/**        -----------------------------------------------------        **/
/**    Questa classe ha il semplice scopo di creare un listing HTML     **/
/**    automatico ed auto-aggiornato delle ultime releases del sito     **/
/**    Punto-Informatico.It scaricando direttamente da esso i feeds     **/
/**    RSS desiderati  ed importandoci un  foglio  XSL per l'output     **/
/**    sfruttando le libxslt  tramite l'estensione  mod_xslt di PHP     **/
/**        -----------------------------------------------------        **/
/**    Per poter utilizzare  questa classe basta  solo scegliere la     **/
/**    rubrica da  scaricare e se visualizzare  l'immagine allegata     **/
/**    nelle apposite variabili rubrica e immagine a inizio script.     **/
/**                                                                     **/
/**                        Rubriche disponibili                         **/
/**                           --------------                            **/
/**              (uo) Ultima ora :: (pi) Notizie del giorno             **/
/**                  (te) Telefonia :: (do) Downloads                   **/
/**        -----------------------------------------------------        **/
/**                                                                     **/
/**          # Creo l'oggetto news                                      **/
/**          $news = new PInews("pi");                                  **/
/**                                                                     **/
/**          # Estraggo le informazioni dal feed e genero l'output      **/
/**          print $news->getPInews();                                  **/
/**                                                                     **/
/**        -----------------------------------------------------        **/
/**                                                                     **/
/**    Ho inserito inoltre un id e delle classi per poterci applicare   **/
/**    delle trasformazioni usando un CSS (Cascading Style Sheets)      **/
/**                                                                     **/
/**                      CSS                    TAG HTML                **/
/**                  ----------                 --------                **/
/**          #PI_RSS_Reader                        dl                   **/
/**          #PI_RSS_Reader_Index_Title            di (Rubrica)         **/
/**          #PI_RSS_Reader_Index_Image            img (Rubrica)        **/
/**          .PI_RSS_Reader_Index                  di (Articolo)        **/
/**          .PI_RSS_Reader_Description            dd (Articolo)        **/
/**                                                                     **/
/**        -----------------------------------------------------        **/
/**    Copyright (C) 2007  Filippo Baruffaldi (www.baruffaldi.info)     **/
/**                                                                     **/
/**    This program is free software; you can redistribute it and/or    **/
/**    modify it under the terms of the GNU General Public License      **/
/**    as published by the Free Software Foundation; either version 2   **/
/**    of the License, or (at your option) any later version.           **/
/**                                                                     **/
/**    This program is distributed in the hope that it will be useful,  **/
/**    but WITHOUT ANY WARRANTY; without even the implied warranty of   **/
/**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    **/
/**    GNU General Public License for more details.                     **/
/**                                                                     **/
/**    You can find a copy at this address: http://www.gnu.org/licenses **/
/*************************************************************************/
/*************************************************************************/

$rubrica = "pi";		// Scegli la rubrica da visualizzare
$immagine = true;		// Vuoi che venga visualizzata anche l'immagine di PI ?

//----[code.snippet]-----

if ($immagine) $immagine = '<a href="{rss/channel/image/link}" title="{rss/channel/image/title}"><img style="width: {rss/channel/image/width}px; height: {rss/channel/image/height}px; border: none;" id="PI_RSS_Reader_Index_Image" src="{rss/channel/image/url}" alt="{rss/channel/title}" /></a>';

$xml = new DOMDocument;
$xml->load("http://punto-informatico.it/fader/$rubrica" . "xml.xml");

$xsl = new DOMDocument;
$xsl->loadXML('
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

		<xsl:template match="/">
						<dl id="PI_RSS_Reader">
								'.$immagine.'
								<xsl:comment>
										<xsl:text>Rubrica: </xsl:text><xsl:value-of select="rss/channel/title" />
								</xsl:comment>
								<di id="PI_RSS_Reader_Index_Title"><h3><xsl:value-of select="rss/channel/title" /></h3></di>
								<dd><br /></dd>
								<xsl:apply-templates select="rss/channel" />
						</dl>
		</xsl:template>

		<xsl:template match="rss/channel">
						<xsl:for-each select="item">
								<xsl:comment>
										<xsl:text>Articolo: </xsl:text><xsl:value-of select="title" />
								</xsl:comment>
								<di class="PI_RSS_Reader_Index"><a href="{link}" title="{title}"><xsl:value-of select="title" /></a></di>
								<dd class="PI_RSS_Reader_Description"><xsl:value-of select="description" /></dd>
								<dd><br /></dd>
						</xsl:for-each>
		</xsl:template>

</xsl:stylesheet>
');

$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl);

$obj = $proc->transformToDoc($xml);

print $obj->saveHTML();
?>


