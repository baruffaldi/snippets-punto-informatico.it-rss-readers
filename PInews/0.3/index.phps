<?php
/*************************************************************************/
/*************************************************************************/
/**           Punto-Informatico.It RegExp Releases Roller v0.3          **/
/**        -----------------------------------------------------        **/
/**    Questa classe ha il semplice scopo di creare un listing HTML     **/
/**    automatico ed auto-aggiornato delle ultime releases del sito     **/
/**    Punto-Informatico.It scaricando direttamente da esso i feeds     **/
/**    RSS desiderati.                                                  **/
/**        -----------------------------------------------------        **/
/**    Per poter utilizzare questa classe basta solo specificare la     **/
/**    rubrica da scaricare come argomento della funzione PInews.       **/
/**                                                                     **/
/**                 (uo) Ultima ora :: (pi) Notizie del giorno          **/
/**                  (te) Telefonia :: (do) Downloads                   **/
/**        -----------------------------------------------------        **/
/**                                                                     **/
/**          # Creo l'oggetto news                                      **/
/**          $news = new PInews("pi");                                  **/
/**                                                                     **/
/**          # Estraggo le informazioni dal feed e genero l'output      **/
/**          print $news->getPInews();                                  **/
/**                                                                     **/
/**        -----------------------------------------------------        **/
/**                                                                     **/
/**    Ho inserito inoltre un id e delle classi per poterci applicare   **/
/**    delle trasformazioni usando un CSS (Cascading Style Sheets)      **/
/**                                                                     **/
/**                      CSS                    TAG HTML                **/
/**                  ----------                 --------                **/
/**          #PI_RSS_Reader                        dl                   **/
/**          .PI_RSS_Reader_Index_Title            di (Rubrica)         **/
/**          .PI_RSS_Reader_Index                  di (Articolo)        **/
/**          .PI_RSS_Reader_Description            dd (Articolo)        **/
/**                                                                     **/
/**        -----------------------------------------------------        **/
/**    Copyright (C) 2007  Filippo Baruffaldi (www.baruffaldi.info)     **/
/**                                                                     **/
/**    This program is free software; you can redistribute it and/or    **/
/**    modify it under the terms of the GNU General Public License      **/
/**    as published by the Free Software Foundation; either version 2   **/
/**    of the License, or (at your option) any later version.           **/
/**                                                                     **/
/**    This program is distributed in the hope that it will be useful,  **/
/**    but WITHOUT ANY WARRANTY; without even the implied warranty of   **/
/**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    **/
/**    GNU General Public License for more details.                     **/
/**                                                                     **/
/**    You can find a copy at this address: http://www.gnu.org/licenses **/
/*************************************************************************/
/*************************************************************************/

$news = new PInews("pi");

print $news->getPInews();

//----[code.snippet]-----

class PInews
{
		var $rubriche = array(
						"pi" => "Punto Informatico :: Notizie del giorno",
						"uo" => "Punto Informatico :: Ultima ora",
						"te" => "Punto Informatico :: Telefonia",
						"do" => "Punto Informatico :: Downloads"
						);
						
		//---[do.not.edit.below]----
		var $news;
		var $rubrica;
		var $titles = array();
		var $links = array();
		var $descriptions = array();

		function PInews($sz)
		{
				$this->news = @fopen("http://punto-informatico.it/fader/$sz" . "xml.xml","r");
				$this->rubrica = $sz;
		}

		function getPInews()
		{
				if (!$this->news) return "Errore: non � stato possibile connettersi al sito di Punto Informatico.";

				while (!feof($this->news))
				{
						$buffer = fgets($this->news, 4096);

						if (preg_match("</item>", $buffer))
						{ $check = 0; }

						elseif ($check)
						{	   preg_match_all("/(<([\w]+)[^>]*>)(.*)(<\/\\2>)/", $buffer, $matches);
								switch ($matches[1][0])
								{
										case "<title>":
											$this->titles[] = $matches[3][0];
											break;

										case "<link>":
											$this->links[] = $matches[3][0];
											break;

										case "<description>":
											$this->descriptions[] = $matches[3][0];
											break;
								} }

						elseif (preg_match("<item>", $buffer)) {
								$check = 1; }
				}

				fclose ($this->news);
				
				$ret = "
				<dl id=\"PI_RSS_Reader\">
						<!-- Rubrica: {$this->rubriche[$this->rubrica]} -->
						<di class=\"PI_RSS_Reader_Index_Title\"><h3>{$this->rubriche[$this->rubrica]}</h3></di>\n
						<dd><br /></dd>\n
						";

				for ($c = 0; $c < count($this->titles); $c++) {
						$ret .= "
						<!-- Articolo: {$this->titles[$c]} -->
						<di class=\"PI_RSS_Reader_Index\"><a href=\"{$this->links[$c]}\" title=\"{$this->titles[$c]}\">" . $this->titles[$c] . "</a></di>\n
						<dd class=\"PI_RSS_Reader_Description\">" . $this->descriptions[$c] . "</dd>\n
						<dd><br /></dd>\n
						";
				}

				$ret .= "
				</dl>
						";

				return $ret;
		}
}
?>

